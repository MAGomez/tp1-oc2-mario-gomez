extern printf

section .data
;msg db "el valor de a es : %.2f",10,0
;msg1 db "el valor de b es : %.2f",10,0
;msg2 db "el valor de c es : %.2f",10,0
    
formato1 db "la primer raiz es : %f ", 10,13,0
formato2 db "la segunda raiz es : %f ", 10,13,0
a dq 1.0
b dq -4.0
c dq 3.0

cte dq 2.0
cte1 dq -4.0

raiz1 dq 0.0
raiz2 dq 0.0

section .bss

section .text
    global resolvente

resolvente:
   
    push ebp
    mov ebp, esp
            
    fld dword[ebp + 8]
    fst qword[a]
    fld dword[ebp + 12]
    fst qword[b]
    fld dword[ebp + 16]
    fst qword[c]

    mov esp, ebp
    pop ebp
    
    ;-----calculo de las raices -----
    
    
    ;------raiz 1
    FLD qword[b]   ;b.b
  FLD qword[b]
  fmul
  FLD qword[cte1] ; -4ac
  FLD qword[a]  
  fmul  
  FLD qword[c]
  fmul
  fadd
  fsqrt           ; saco raiz de todo lo de arriba
  FLD qword[b]     ; -b
  fchs
  fadd
  FLD qword[cte]    ; divido por 2a
  FLD qword[a]
  fmul
  fdiv
  fst qword[raiz1]
  
    ; ---------raiz 2
    FLD qword[b]
  FLD qword[b]
  fmul
  FLD qword[cte1] 
  FLD qword[a]  
  fmul  
  FLD qword[c]
  fmul
  fadd
  fsqrt
  fchs
  FLD qword[b]     
  fchs
  fadd
  FLD qword[cte]
  FLD qword[a]
  fmul
  fdiv
  fst qword[raiz2]
  
    ;---mostrar las raices
    push dword[raiz1+4]   
    push dword[raiz1]
    push formato1
    call printf
    add esp,12
 
    push dword[raiz2+4]   
    push dword[raiz2]
    push formato2
    call printf
    add esp,12
  
    xor eax, eax 
    
ret