%include "io.inc"
extern _printf
section .data

formato1 db "la primer raiz es : %f ", 10,13,0
formato2 db "la segunda raiz es : %f ", 10,13,0
a dq 1.0       ; coeficiente a
b dq 0.0       ;coeficiente b
c dq -3.0      ; coeficiente c

cte dq 2.0     ; 2
cte1 dq -4.0   ; -4

raiz1 dq 0.0
raiz2 dq 0.0

section .text

global CMAIN
CMAIN:
   mov ebp, esp; for correct debugging 
    
  ;calculo de la primer raíz
  FLD qword[b]   ;b.b
  FLD qword[b]
  fmul
  FLD qword[cte1] ; -4ac
  FLD qword[a]  
  fmul  
  FLD qword[c]
  fmul
  fadd
  fsqrt           ; saco raiz de todo lo de arriba
  FLD qword[b]     ; -b
  fchs
  fadd
  FLD qword[cte]    ; divido por 2a
  FLD qword[a]
  fmul
  fdiv
  fst qword[raiz1]
 
  ;- calculo de la segunda raíz
  FLD qword[b]
  FLD qword[b]
  fmul
  FLD qword[cte1] 
  FLD qword[a]  
  fmul  
  FLD qword[c]
  fmul
  fadd
  fsqrt
  fchs
  FLD qword[b]     ; -b
  fchs
  fadd
  FLD qword[cte]
  FLD qword[a]
  fmul
  fdiv
  fst qword[raiz2]
  
  
  ;---mostrar las raices
  push dword[raiz1+4]   
  push dword[raiz1]
  push formato1
  call printf
  add esp,12
 
  push dword[raiz2+4]   
  push dword[raiz2]
  push formato2
  call printf
  add esp,12
  
  xor eax, eax 
  ret