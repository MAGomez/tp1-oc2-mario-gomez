# **Informe de OC2**

### **Punto 1) programa en assembler**

En este apartado, sólo se muestra el código que encuentra las raices en Assembler, los casos que sirven de prueba.  
Los resultados se mostraran mas adelante. 
El archivo que contiene este codigo se llama **Punto1Resolvente.asm** 
Pruebas para encontrar las raíces de ax^2 + bx +c:

* a= 1  b= -4  c= 3   y tienen que dar r1= 3  r2= 1
* a= 1  b=  4  c= 3   y tienen que dar r1= -3 r2= -1
* a= 1  b= 0   c= 0   y tienen que dar r1= 0  r2= 0
* a= 1  b= -4  c= 4   y tienen que dar r1= 2  r2= 2
* a= 1  b=  0  c= -3  y tienen que dar r1= 1,7320 r2= -1,7320

```s
%include "io.inc"
extern _printf
section .data

formato1 db "la primer raiz es : %f ", 10,13,0
formato2 db "la segunda raiz es : %f ", 10,13,0

a dq 1.0       ; coeficiente a
b dq 0.0       ;coeficiente b
c dq -3.0      ; coeficiente c

cte dq 2.0     ; 2
cte1 dq -4.0   ; -4

raiz1 dq 0.0
raiz2 dq 0.0

section .text

global CMAIN

CMAIN:
   mov ebp, esp; for correct debugging 
    
  ;calculo de la primer raíz
  FLD qword[b]   ;b.b
  FLD qword[b]
  fmul
  FLD qword[cte1] ; -4ac
  FLD qword[a]  
  fmul  
  FLD qword[c]
  fmul
  fadd
  fsqrt           ; saco raíz de todo lo de arriba
  FLD qword[b]     ; -b
  fchs
  fadd
  FLD qword[cte]    ; divido por 2a
  FLD qword[a]
  fmul
  fdiv
  fst qword[raiz1]
  
  ;- calculo de la segunda raíz
  FLD qword[b]
  FLD qword[b]
  fmul
  FLD qword[cte1] 
  FLD qword[a]  
  fmul  
  FLD qword[c]
  fmul
  fadd
  fsqrt
  fchs
  FLD qword[b]     ; -b
  fchs
  fadd
  FLD qword[cte]
  FLD qword[a]
  fmul
  fdiv
  fst qword[raiz2]
  
 
  ;---mostrar las raices
  push dword[raiz1+4]  
  push dword[raiz1]
  push formato1
  call printf
  add esp,12
  
  push dword[raiz2+4]  
  push dword[raiz2]
  push formato2
  call printf
  add esp,12
  
  xor eax, eax 
  ret 
```

### **Punto 2) programa en C**

El programa lo que hace es pedir si quiere ingresar coeficientes Enteros o Reales, luego en el caso de que haya ingresado números Enteros, lo interpreta como un número de punto flotante. 
Aquí se puede apreciar el código en *C* que luego se unira al archivo objeto que surge de compilar el archivo en Assembler. 
El archivo que contiene este código se llama **resolvente.c**   
 
```C
#include <stdio.h>
#include <stdlib.h>
extern void resolvente(float a, float b, float c); 


int main() 
{
    
  int x;
  int y;
  int z;
  float a;
  float b;
  float c;
  int eleccion;
  printf("calculo de las raices de ax2 +bc +c \n");
  printf("Si quiere las raices enteras tocar 1, si quiere las raices reales toque 2, sino sale\n");
  scanf ("%d",&eleccion);
  if (eleccion ==1){
     printf("calculo de las raices Enteras de ax2 +bc +c\n"); 
     printf("Ingrese el valo de a\n");
     scanf ("%d",&x);
     a = (float) x;
     printf("ingrese el valor de b\n");
     scanf ("%d",&y);
     b = (float) y;
     printf("ingrese el valor de c\n");
     scanf ("%d",&z);
     c =(float) z;
    
    printf ("los valores cargados son : %d **  %d  ** %d \n",x,y,z);
     resolvente(x, y, z);
    return 0;
}else if(eleccion == 2){
     printf("calculo de las raices Reales de ax2 +bc +c\n"); 
     printf("Ingrese el valo de a\n");
     scanf ("%f",&a);
     printf("ingrese el valor de b\n");
     scanf ("%f",&b);
     printf("ingrese el valor de c\n");
     scanf ("%f",&c);
    
    printf ("los valores cargados son: %f  ** %f  ** %f \n",a,b,c);
    resolvente(a, b, c);
    return 0;
        }else {
            return 0;
    }

}
```

### *Punto 3) archivo .sh*

Se deja un archivo para que se ejecute directamente el programa para calcular las raices de un polinomio de grado 2. Los pasos que se realizan son :  
1. compilar el **resolvente.s**
2. linkear  archivo en **resolvente.c** y el **resolvente.o**
3. ejecutar el archivo ejecutable **resolver**

*Capturas de pruebas en nasm*  
![imagen 1](https://gitlab.com/MAGomez/tp1-oc2-mario-gomez/-/blob/master/cap1.png)  
![imagen 2](https://gitlab.com/MAGomez/tp1-oc2-mario-gomez/-/blob/master/cap2.png)  
![imagen 3](https://gitlab.com/MAGomez/tp1-oc2-mario-gomez/-/blob/master/cap3.png)  
![imagen 4](https://gitlab.com/MAGomez/tp1-oc2-mario-gomez/-/blob/master/cap4.png)  
![imagen 5](https://gitlab.com/MAGomez/tp1-oc2-mario-gomez/-/blob/master/cap1.png)   

*Capturas del ejecutable*     
![imagen 1](https://gitlab.com/MAGomez/tp1-oc2-mario-gomez/-/blob/master/cap1vs.png)  
![imagen 2](https://gitlab.com/MAGomez/tp1-oc2-mario-gomez/-/blob/master/cap2vs.png)  
![imagen 3](https://gitlab.com/MAGomez/tp1-oc2-mario-gomez/-/blob/master/cap3vs.png)  
![imagen 4](https://gitlab.com/MAGomez/tp1-oc2-mario-gomez/-/blob/master/cap4vs.png)  
![imagen 5](https://gitlab.com/MAGomez/tp1-oc2-mario-gomez/-/blob/master/cap5vs.png)  


## *Ejercicios obligatorios*

### *Ejercicio 4 - gestión de memoria*

*datos:*  
32 bits dirección virtual  
1 GB de RAM  
4 KB tamaño de frame 

*Resolución:*

Como el tamño de un  frame es igual al tamaño de una página.  
página  -> 4 KB = 2^2 .2^10 = 2^12 -> 12 lo tomo como ofsset.  
formato de dirección virtual <20 , 12>    20+12 =32 bits.  

Como tengo 1 GB de Ram, entonces 1 GB = 2^30  tomo el 30 como el tamaño de las direcciones físicas (30 bits).  
formato de dirección física  <18 , 12>    18+12=30 bits.


A) Usando páginación de un nivel, puedo direccionar 2^20 páginas.

B) Usando paginación de nivel invertido, puedo direccionar 2^18 páginas.
    
### *ejercicio 6 - gestión de memoria*

CS-> base 1000, límite 1800.  
DS-> base 500, límite 750.  
SS-> base 4000, límite 4200.

Para encontrar la direccíon física a la dirección base le sumo la dirección de DS, SC o SS dada y me fijo si no 
sobrepaso el límite del registro.

A) Dirección 0 para DS -> dirección física 500.

B) Dirección 550 para SC -> dirección física 1550.

C) Dirección 100 para SS -> dirección física 4100.

D) Dirección 4000 para SS -> dirección física 8000 (dirección invalida porque se paso del límite).

### *ejercicio 7 - gestión de memoria*
Punto A) Estado final  
TLB  

| página | Frame | Tiempo |
| ------ | ------ | ------ |
| 4 | 3 |  0 |
| 5 | 4 | 1 |
   

Tabla de Páginas  


| Página | Frame | Valid | Tiempo |
| ------ | ------ | ------ | ------ |
| 1 | 1 | v | 1 |
| 2 | 2 | v | 0 |
| 3 | - | i | - |
| 4 | 3 | v | 2 |
| 5 | 4 | v | 3 |
| 6 | - | i | - |

    

Memoria Principal 

| Frame1 | Frame2 | Frame3 | Frame4 |
| ------ | ------ | ------ | ------ |
| 1 | 2 | 4 | 5 |
 

Backing Store  

| Página | Página |
| ------ | ------ |
| 3 | 6 |


Secuencia de tiempo 
 
| Página | Tiempo | Tiempo Acumulado |
| ------ | ------ | ------ |
| 1 | 1 | 1 |
| 2 | 1 | 2 |
| 6 | 3 | 5 |
| 3 | 13 | 18 |
| 2 | 13 | 31 |
| 1 | 13 | 44 |
| 4 | 13 | 57 |
| 5 | 13 | 70 |
  
  Punto B) Estado final  
  TLB  

| página | Frame | Tiempo |  
| ------ | ------ | ------ |  
| 5 | 3 |  0 |  
| 6 | 4 | 1 |  


Tabla de Páginas 

| Página | Frame | Valid | Tiempo |
| ------ | ------ | ------ | ------ |
| 1 | 1 | i | - |
| 2 | 2 | v | 0 |
| 3 | - | i | - |
| 4 | 1 | v | 1 |
| 5 | 3 | v | 2 |
| 6 | 4 | v | 3 |


Memoria Principal 

| Frame1 | Frame2 | Frame3 | Frame4 |
| ------ | ------ | ------ | ------ |
| 4 | 2 | 5 | 6 |


Backing Store

| Página | Página |
| ------ | ------ |
| 1 | 3 |
 
Secuencia de tiempo 

  | Página | Tiempo | Tiempo Acumulado |
  | ------ | ----- | ----------------- |  
  |    6    |1+2         |  3                 |  
  |    1    |1           |   4                |  
  |  3      |  1+2+10    |   17       |
  |   2     |1+2+10      |  30                 |  
  |   4     |1+2+10      |43       |  
  |    5    |1+2+10      | 56       |
  |   4     | 1          |    57               |
  |  6      |1+2+10      |70     |  

### *ejercicio 4 - fpu*

```s
%include "io.inc"
section .data
msg dd "la suma es  : %f ", 10,13,0
arr dq 1.0,2.0,3.0,4.0,12.0   ;  la suma da 15
resultado dq 0.0    ;voy guardando la suma de los valores en cada posicion
tamanio dq 5    ;cuantos elementos tiene el arreglo
  
section .text
global CMAIN
CMAIN:
    mov ebp, esp; for correct debugging
    xor ecx, ecx  ;cuenta las iteraciones
    xor edx, edx  ; puntero del arreglo
    ; float suma_vf(float *vector, int cantidad)
    
     push ebp      ; enter
     mov ebp, esp
      
        FLD qword[resultado]    ; subo el resultado parcial
        FLD qword[arr]        ;subo el valor segun posicion del arreglo
        fadd 
        fst qword[resultado] 
        inc ecx 
        add edx,8  ;;;;;;antes era 4 con dd
    ciclo:
    
        cmp [tamanio],ecx       ; ver si recorrio todo el vector
        jz terminar            ; si son iguales o sea llego al último elementos
       
        
        FLD qword[resultado]    ; subo el resultado parcial
        FLD qword[arr+edx]      ;subo el valor segun posicion del arreglo
        fadd                    ; sumo
        fst qword[resultado]    ; guardo en resultado
      
        inc ecx  
        add edx,8      ;;; antes era 4 con dd         
        jmp ciclo                        
       
    terminar:
        
        push dword[resultado+4]    ; mostrar el resultado
        push dword[resultado]
        push msg
        call printf
        add esp,12
        
        mov ebp,esp     ; leave
        pop esp
        xor eax, eax    
        ret
   
```
